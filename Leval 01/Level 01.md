# Nivel 01

Bem-vindo ao Flexbox Froggy, um jogo onde você ajuda Froggy e amigos escrevendo código CSS!<br/>
 Guie este sapo para o lilypad à direita usando a propriedade, que alinha os itens horizontalmente e aceita os seguintes valores:`justify-content`

`flex-start`: Os itens se alinham ao lado esquerdo do contêiner.<br/>
`flex-end`: Os itens se alinham ao lado direito do contêiner.<br/>
`center`: Os itens se alinham no centro do contêiner.<br/>
`space-`: Os itens são exibidos com espaçamento igual entre eles.<br/>
`space-around`: Os itens são exibidos com espaçamento igual ao redor deles.<br/>
Por exemplo, vai mover o sapo para a direita. `justify-content: flex-end;`